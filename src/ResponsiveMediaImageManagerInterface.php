<?php

namespace Drupal\responsive_media_image;

/**
 * Interface ResponsiveMediaImageManagerInterface.
 */
interface ResponsiveMediaImageManagerInterface {

  const AUTOMATIC = 'automatic';

  const AUTOMATIC_CACHE_KEY = 'responsive_media_image_automatic';

  /**
   * Gets all image field types for the media entity.
   *
   * @return array
   *   An array of field storage IDs
   */
  public function getMediaImageFields();

  /**
   * Gets the target image field of the media entity.
   *
   * @param \Drupal\media\MediaInterface $media
   *     The media entity.
   *
   * @return \Drupal\Core\Field\EntityReferenceFieldItemListInterface|FALSE
   *     The field if it exists, FALSE otherwise.
   */
  public function getImageField(\Drupal\media\MediaInterface $media);
}
