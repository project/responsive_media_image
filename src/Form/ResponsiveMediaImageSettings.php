<?php

namespace Drupal\responsive_media_image\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\responsive_media_image\ResponsiveMediaImageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ResponsiveMediaImageSettings.
 */
class ResponsiveMediaImageSettings extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The responsive media image manager
   *
   * @var \Drupal\responsive_media_image\ResponsiveMediaImageManagerInterface
   */
  protected $responsiveMediaImageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    $instance->responsiveMediaImageManager = $container->get('responsive_media_image.manager');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['responsive_media_image.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'responsive_media_image_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $image_fields = $this->responsiveMediaImageManager->getMediaImageFields();
    $config = $this->config('responsive_media_image.settings');

    $options = [ResponsiveMediaImageManagerInterface::AUTOMATIC => $this->t('- Automatic -')] + $image_fields;

    $form['image_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Field'),
      '#description' => $this->t('Select the image field so the formatter can find the image and convert it to responsive image.
      Automatic means that the formatter will try to find the image by itself'),
      '#options' => $options,
      '#default_value' => $config->get('image_field'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    $config = $this->config('responsive_media_image.settings');
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value);
    }

    $config->save();
    parent::submitForm($form, $form_state);
  }
}
