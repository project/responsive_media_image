<?php

namespace Drupal\responsive_media_image\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\Entity\File;
use Drupal\media\MediaInterface;
use Drupal\responsive_image\Plugin\Field\FieldFormatter\ResponsiveImageFormatter;
use Drupal\responsive_media_image\ResponsiveMediaImageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'responsive_media_thumbnail' formatter.
 *
 * @FieldFormatter(
 *   id = "responsive_media_thumbnail",
 *   label = @Translation("Responsive Media thumbnail"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class ResponsiveMediaThumbnailFormatter extends ResponsiveImageFormatter implements ContainerFactoryPluginInterface {

  /**
   * The responsive media image manager
   *
   * @var \Drupal\responsive_media_image\ResponsiveMediaImageManagerInterface
   */
  protected $responsiveMediaImageManager;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->responsiveMediaImageManager = $container->get('responsive_media_image.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * This has to be overridden because FileFormatterBase expects $item to be
   * of type \Drupal\file\Plugin\Field\FieldType\FileItem and calls
   * isDisplayed() which is not in FieldItemInterface.
   */
  protected function needsEntityLoad(EntityReferenceItem $item) {
    return !$item->hasNewEntity();
  }

  /**
   * {@inheritDoc}
   *
   * Alter the options for image_link. Change the 'content' label to 'Media', andd add a new option 'current_content' so we can get
   * The url of the current entity, not the media entity.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['image_link']['#options']['content'] = $this->t('Media');
    $elements['image_link']['#options']['current_content'] = $this->t('Content');
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for entity types that reference
    // media items.
    return ($field_definition->getFieldStorageDefinition()->getSetting('target_type') == 'media');
  }

  /**
   * This function is like prepareView, but is hard coded to load file entities. Normally, prepareView loads entities by using
   * the current formatter entity type which is 'media', but we don't want that, we want 'file' entities.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The item list
   *
   * @return void
   */
  protected function prepareImageFieldView(\Drupal\Core\Field\FieldItemListInterface $items) {
    foreach ($items as $item) {
      $file_id = $item->target_id;
      $file = File::load($file_id);
      if ($file) {
        $item->_loaded = TRUE;
        $item->entity = $file;
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $media_entities = $this->getEntitiesToView($items, $langcode);
    /** @var \Drupal\media\MediaInterface[] $media_entities */
    foreach ($media_entities as $delta => $media) {
      // Get the image field.
      $image_field_item_list = $this->responsiveMediaImageManager->getImageField($media);
      if (!$image_field_item_list) {
        continue;
      }

      $this->prepareImageFieldView($image_field_item_list);
      $element[$delta] = parent::viewElements($image_field_item_list, $langcode);

      // If the image link is 'current_content' we need to do some magic and overridding.
      $image_link = $this->getSetting('image_link');
      if ($image_link === 'current_content') {
        foreach ($element[$delta] as &$responsive_image_element) {
          $responsive_image_element['#url'] = $items->getEntity()->toUrl();
        }
      }
    }

    // Lastly, depend on the module config.
    $element['#cache']['tags'] = ['config:responsive_media_image.settings'];

    return $element;
  }
}
