<?php

namespace Drupal\responsive_media_image;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\media\MediaInterface;

/**
 * Class ResponsiveMediaImageManager.
 */
class ResponsiveMediaImageManager implements ResponsiveMediaImageManagerInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The responsive media image configurations
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The default cache bin
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The static cache object
   *
   * @var object
   */
  protected $staticCache;

  /**
   * Constructs a new ResponsiveMediaImageManager object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, ConfigFactoryInterface $config_factory, \Drupal\Core\Cache\CacheBackendInterface $cache) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->config = $config_factory->get('responsive_media_image.settings');
    $this->cache = $cache;
  }

  /**
   * {@inheritDoc}
   */
  public function getMediaImageFields() {
    $options = [];
    $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions('media');

    foreach ($field_storage_definitions as $field_name => $definition) {
      // We check for BaseFieldDefinition because we want to ignore the 'thumbnail' field.
      if ($definition->getType() === 'image' && !$definition->isBaseField()) {
        $options[$field_name] = sprintf("%s (%s)", $definition->getLabel(), $field_name);
      }
    }

    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function getImageField(MediaInterface $media) {
    $image_field = $this->config->get('image_field');

    // No image field? return FALSE.
    if (!$image_field) {
      return FALSE;
    }

    if ($image_field === self::AUTOMATIC) {
      // Find the image field automatically.

      // Check the cache first, we check for the static first then the backend cache.
      if (($cache = $this->staticCache) || ($cache = $this->cache->get(self::AUTOMATIC_CACHE_KEY))) {
        $this->staticCache = $cache;
        $image_field = $cache->data;
        if ($media->hasField($image_field)) {
          return $media->get($image_field);
        } else {
          $this->staticCache = NULL;
          $this->cache->delete(self::AUTOMATIC_CACHE_KEY);
        }
      }

      // If we failed to get from the default cache, try to find the image field.
      $media_bundle_fields = $this->entityFieldManager->getFieldDefinitions('media', $media->bundle());

      foreach ($media_bundle_fields as $field_name => $definition) {
        // We check for BaseFieldDefinition because we want to ignore the 'thumbnail' field.
        if ($definition->getType() === 'image' && !($definition instanceof BaseFieldDefinition)) {
          $this->cache->set(self::AUTOMATIC_CACHE_KEY, $field_name, CacheBackendInterface::CACHE_PERMANENT, ['config:responsive_media_image.settings']);
          $this->staticCache = $this->cache->get(self::AUTOMATIC_CACHE_KEY);
          return $media->get($field_name);
        }
      }
    } else if ($media->hasField($image_field)) {
      // Get the image field directly.
      return $media->get($image_field);
    }

    return FALSE;
  }

}
